extern crate serde_derive;
extern crate serde_json;
use crate::errors;


use serde_derive::{Deserialize, Serialize};
use std::cmp::Ordering;
use std::collections::HashMap;
use std::collections::HashSet;
use std::hash::{Hash, Hasher};

use std::io::BufReader;
use std::fs::File;


//required impls
impl PartialEq for Ingredient {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name
    }
}

impl Eq for Ingredient {}

impl Hash for Ingredient {
    fn hash<H: Hasher>(&self, h: &mut H) {
        self.name.hash(h);
    }
}

impl PartialOrd for Ingredient {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.name.partial_cmp(&other.name)
    }
}

impl Ord for Ingredient {
    fn cmp(&self, other: &Self) -> Ordering {
        self.name.cmp(&other.name)
    }
}

//struct definitions
#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum Unit {
    MilliLiter,
    Gram,
    Single,
    DeciLiter,
    HectoGram,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Ingredient {
    pub name:               String,
    pub unit:               Unit,
    pub calories:           f32,
    pub typical_portion:    Option<f32>,
    pub quantity:           Option<f32>,
    pub fat:                Option<f32>,
    pub satfat:             Option<f32>,
    pub carbs:              Option<f32>,
    pub sugar:              Option<f32>,
    pub fibre:              Option<f32>,
    pub protein:            Option<f32>,
    pub salt:               Option<f32>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct IngredientList {
    pub list: HashSet<Ingredient>,
}

//actual impls
impl Ingredient {
    pub fn calories(&self, amount: &f32) -> f32 {
        let multiplier = match self.unit {
            Unit::MilliLiter => 1.0,
            Unit::Gram => 1.0,
            Unit::Single => 1.0,
            Unit::DeciLiter => 0.01,
            Unit::HectoGram => 0.01,
        };

        amount * (multiplier * self.calories)
    }

    pub fn new(name: String,calories: f32,unit: Unit,typical_portion: Option<f32>,
        quantity: Option<f32>,fat: Option<f32>,satfat: Option<f32>,carbs: Option<f32>,
        sugar: Option<f32>,fibre: Option<f32>,
        protein: Option<f32>,salt:  Option<f32>,) -> Ingredient {

        Ingredient {
            name,
            unit,
            calories,
            typical_portion,
            quantity,
            fat,
            satfat,
            carbs,
            sugar,
            fibre,
            protein,
            salt,
        }
    }
}

impl IngredientList {
    pub fn find(&self, key: &str) -> Option<&Ingredient> {
        self.list.iter().find(|&ing| ing.name.eq(key))
    }

    pub fn insert(&mut self, ingredient: Ingredient) -> bool {
        self.list.insert(ingredient)
    }

    pub fn write_to_file(&self, filepath: &std::path::PathBuf)   ->  Result<(), errors::PersonError> {
        let mut writefile = std::fs::File::create(filepath)?;
    
        serde_json::to_writer_pretty(writefile, &self)?;
        
        Ok(())
    }

    
}

//non impl funcs

pub fn read_from_file(filepath: &std::path::PathBuf) ->  Result<IngredientList, errors::PersonError> {
    let file = File::open(filepath)?;
    let reader = BufReader::new(file);

    let p = serde_json::from_reader(reader)?;

    Ok(p)

}

