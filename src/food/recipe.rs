use crate::errors;
extern crate serde_derive;
extern crate serde_json;

use serde_derive::{Deserialize, Serialize};
use std::cmp::Ordering;
use std::collections::HashMap;
use std::collections::HashSet;
use std::fs::File;
use std::hash::{Hash, Hasher};
use std::io::BufReader;

//required impls
impl Eq for PackedRecipe {}

impl Hash for PackedRecipe {
    fn hash<H: Hasher>(&self, h: &mut H) {
        self.name.hash(h);
    }
}

impl PartialOrd for PackedRecipe {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.name.partial_cmp(&other.name)
    }
}

impl Ord for PackedRecipe {
    fn cmp(&self, other: &Self) -> Ordering {
        self.name.cmp(&other.name)
    }
}

impl PartialEq for PackedRecipe {
    fn eq(& self, other: &Self) -> bool {
        self.name == other.name
    }
}

impl Eq for Recipe {}

impl Hash for Recipe {
    fn hash<H: Hasher>(&self, h: &mut H) {
        self.name.hash(h);
    }
}

impl PartialOrd for Recipe {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.name.partial_cmp(&other.name)
    }
}

impl Ord for Recipe {
    fn cmp(&self, other: &Self) -> Ordering {
        self.name.cmp(&other.name)
    }
}

impl PartialEq for Recipe {
    fn eq(& self, other: &Self) -> bool {
        self.name == other.name
    }
}

//struct definitions

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct PackedRecipe {
    pub name: String,
    pub weight: Option<f32>,
    pub ingredients: HashMap<String,f32>,
    pub portions: f32,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct RecipeList {
    pub list: HashSet<PackedRecipe>,
}

#[derive(Debug, Clone)]
pub struct Recipe {
    pub name: String,
    pub weight: Option<f32>,
    pub ingredients: HashMap<super::ingredient::Ingredient,f32>, //references isnt working well, change to owned or COW/RC?
    pub portions: f32,
}

//actual impls

impl PackedRecipe { //this needs to be tidied
    pub fn unpack(self, ingredients_list: & super::ingredient::IngredientList)  ->  Recipe {
        let converted_ingredients: HashMap<super::ingredient::Ingredient, f32> = self.ingredients.iter().map(|(key, &value)| {
            //let op_ing = ingredients_list.find(key);
            (ingredients_list.find(key).unwrap().clone(), value) //implement some error handling here, also check that Rc really does something here
        }).collect();

        Recipe {
            name: self.name,
            ingredients: converted_ingredients,
            portions: self.portions,
            weight: self.weight,
        }
    }
}


impl Recipe {
    pub fn total_calories(&self)    ->  f32 {
        self.ingredients.iter().fold(0.0,|acc, (ing, amount)| acc + ing.calories(amount) )
    }

    pub fn  portion_calories(&self) ->  f32 {
        self.total_calories() / self.portions
    }
    
    pub fn pack(&self)  ->  PackedRecipe {
        PackedRecipe {
            name: self.name.clone(),
            weight: self.weight,
            ingredients: self.ingredients.iter().map(|(key, &value)| (key.name.clone(), value)).collect(),
            portions: self.portions,
        }
    }

}

impl RecipeList {
    pub fn find(&self, key: &str) -> Option<&PackedRecipe> {
        self.list.iter().find(|&rec| rec.name.eq(key))
    }

    pub fn insert(&mut self, recipe: PackedRecipe) -> bool {
        self.list.insert(recipe)
    }

    pub fn write_to_file(&self, filepath: &std::path::PathBuf)   ->  Result<(), errors::PersonError> {
        let mut writefile = std::fs::File::create(filepath)?;
    
        serde_json::to_writer_pretty(writefile, &self)?;
        
        Ok(())
    }
}


