
use crate::errors;
extern crate serde_derive;
extern crate serde_json;

use std::time::{SystemTime, UNIX_EPOCH};
use serde_derive::{Serialize, Deserialize};
use std::fs::File;
use std::io::BufReader;


#[derive(Serialize, Deserialize, Debug)]
pub struct Composition {
    pub weight: Vec<Option<f64>>,
    pub date: Vec<i64>,
    pub bodyfat: Vec<Option<f64>>
}


impl Composition {

    pub fn len(&self)   ->  usize {
        self.date.len()
    }
    
    
    pub fn last_weight(&self)  ->  Option<f64> {
        self.weight[self.len()-1]
    
    }
    
    pub fn last_bodyfat(&self)  ->  Option<f64> {
        self.bodyfat[self.len()-1]
    
    }
    
    pub fn add(&mut self, weight: Option<f64>, bodyfat: Option<f64>)    ->  Result<(), errors::FileError> {
        let currenttime = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH)?.as_secs() as i64;
        self.weight.push(weight);
        self.bodyfat.push(bodyfat);
        self.date.push(currenttime);
        
        Ok(())
    }
    
    pub fn write_to_file(&self, filepath: &std::path::PathBuf)   ->  Result<(), errors::FileError> { //redo this as a trait
        let mut writefile = std::fs::File::create(filepath)?;
    
        serde_json::to_writer_pretty(writefile, &self)?;
        
        Ok(())
    }
    
}

pub fn read_from_file(comp_file: &std::path::PathBuf) ->  Result<Composition, errors::FileError> {
    let file = File::open(comp_file)?;
    let reader = BufReader::new(file);

    let p = serde_json::from_reader(reader)?;

    Ok(p)
}

