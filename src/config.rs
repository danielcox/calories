use crate::errors;
use crate::composition;

extern crate serde_derive;
extern crate serde_json;
extern crate chrono;

use serde_derive::{Serialize, Deserialize};
use std::io::Write;
use std::time::{SystemTime};
use chrono::prelude::*;
use std::fs::File;
use std::io::BufReader;
use rprompt;


#[derive(Serialize, Deserialize, Debug)]
pub enum BiologicalSex {
    Male,
    Female
}

//  STRUCTS //
//person config
#[derive(Serialize, Deserialize, Debug)]
pub struct Person {
    pub birthday: i64,
    pub current_weight: f64,
    pub current_bodyfat: Option<f64>,
    pub initial_weight: f64,
    pub initial_bodyfat: Option<f64>,
    pub sex: BiologicalSex,
    pub height: f64,
    pub generated_date: i64,
    pub activity_level: f64
}

impl Person {

    pub fn tdee(&self)  ->  f64 {
        self.bmr() * self.activity_level
        
    
    }
    
    pub fn bmr(&self)  ->  f64 {
    
        match self.current_bodyfat {
            None        =>  {   //if no bodyfat use Mifflin St Jeor equation
                let age_in_secs = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_secs() as i64 - self.birthday;
                let age_in_years = age_in_secs as f64/31557600.0;
        
                match self.sex {
                    BiologicalSex::Male     =>  10.0*self.current_weight + 6.25*self.height - 5.0*age_in_years + 5.0,
                    BiologicalSex::Female   =>  10.0*self.current_weight + 6.25*self.height - 5.0*age_in_years - 161.0
                }
            },
            Some(bf)    =>  {   //if bodyfat then Katch-McArdle Formula
                370.0 + (21.6*( self.current_weight*(1.0 - (self.current_bodyfat.unwrap()/100.0) ) ) )
            }
        }
        
    
    }
    
    pub fn bmi(&self)   ->  f64 {
        self.current_weight / ( (self.height/100.0).powi(2))
    }
    
    pub fn write_to_file(&self, config_file: &std::path::PathBuf)   ->  Result<(), errors::PersonError> {
        let mut writefile = std::fs::File::create(config_file)?;
    
        serde_json::to_writer_pretty(writefile, &self)?;
        
        Ok(())
    }
    
    


}

pub fn read_from_file(config_file: &std::path::PathBuf) ->  Result<Person, errors::PersonError> {
        let file = File::open(config_file)?;
        let reader = BufReader::new(file);

        let p = serde_json::from_reader(reader)?;

        Ok(p)
    
    }



//check if config directory exists
pub fn check_config_dir()   ->  Option<std::path::PathBuf> {
    let mut config_dir :std::path::PathBuf = dirs::config_dir().expect("No user config directory found");
    config_dir.push("calories");

    match config_dir.is_dir() {
        true    =>  Some(config_dir),
        false   =>  None
    }
}

//make config directory
pub fn make_config_dir(config_path :&std::path::PathBuf) ->  Result<(), std::io::Error> {
    std::fs::create_dir(config_path)
}

fn remove_newline(s: String) -> String {
    //convenient function that removes newline characters
    s.trim_end().to_string()
}

//make a new config file and ask the user for the inputs
pub fn create_config_interactive(config_file: &std::path::PathBuf, comp_file: &std::path::PathBuf)  ->  Result<(), errors::PersonError>  {

    println!("Config file not found, please enter details");


    //todo: get rid of individual birthday entering and use parse_birthday_input instead. Shore up parse_birthday_input to use proper errors
    //and do bounds checking for birthdays
    
    let birthdate;
    
    loop {
    
    let user_birthdate: String = rprompt::prompt_reply_stdout("Birthdate: ").unwrap();


    birthdate = match parse_birthday_input(&user_birthdate) {
        Ok((d,m,y))                       =>  { (d,m,y) },
        Err(err)    =>  match err {
            errors::BirthdayParseError::Format{ .. }   => { eprintln!("Not an integer"); continue },
            _                       =>  panic!("other"),
        }
    };
    
    
        
    break;

    }
    
    
    
        let birthday_chrono = Local.ymd(birthdate.2, birthdate.1, birthdate.0).and_hms(0,0,0);



    let current_date = Local::now();
    println!("Current age: {:?}", (current_date.timestamp() - birthday_chrono.timestamp())/31536000 );
    println!("Birth: {}/{}/{}", birthdate.0, birthdate.1, birthdate.2);
    
    
    
    //do it again for biological sex
    let mut sex :BiologicalSex;
    
    loop {
    
    let output = match cli_parse("Sex [M/F]") {
        Ok(s)   =>  s,
        Err(e)  =>  panic!("Error: Cannot read input"),
    };
    
    
    
    sex = match parse_sex_input(&output) {
        Ok(sex)     =>  sex,
        Err(err)    =>  match err {
            errors::InputParseError::NoMatch{ .. }   => { eprintln!("Not a valid input (choose from [M/F])"); continue },
            _                       =>  panic!("other"),
        }
    };
    
    break;
    }
    
    //do it again for height
    
    let mut height :u8 = 170;
    
    loop {
    
    let output = match cli_parse("Height [cm]") {
        Ok(s)   =>  s,
        Err(e)  =>  panic!("Error: Cannot read input"),
    };
    
    
    
    height = match parse_height_input(&output) {
        Ok(h)     =>  h,
        Err(err)    =>  match err {
            errors::InputParseError::IntFormat{ .. }   => { eprintln!("Not a valid number"); continue },
            _                       =>  panic!("other"),
        }
    };
    
    break;
    }
    
    //do it again for current weight
    
    let mut current_weight :f64 = 75.0;
    
    loop {
    
    let output = match cli_parse("Weight [kg]") {
        Ok(s)   =>  s,
        Err(e)  =>  panic!("Error: Cannot read input"),
    };
    
    
    
    current_weight = match parse_weight_input(&output) {
        Ok(w)     =>  w,
        Err(err)    =>  match err {
            errors::InputParseError::FloatFormat{ .. }   => { eprintln!("Not a valid number"); continue },
            _                       =>  panic!("other"),
        }
    };
    
    break;
    }
    
    let mut current_bodyfat :Option<f64> = None;
    
    loop {
    
    let output = match cli_parse("Bodyfat percentage (if known) [%]") {
        Ok(s)   =>  s,
        Err(e)  =>  panic!("Error: Cannot read input"),
    };
    
    
    
    current_bodyfat = match parse_weight_input(&output) {
        Ok(w)     =>  Some(w),
        Err(err)    =>  match err {
            errors::InputParseError::FloatFormat{ .. }   => { eprintln!("Not a valid number"); continue },
            errors::InputParseError::Empty              =>  None,
            _                       =>  panic!("other"),
        }
    };
    
    
    break;
    }
    
    let person_config = Person{ birthday: birthday_chrono.timestamp(), current_weight: current_weight, sex: sex, height: height as f64, generated_date: Local::now().timestamp(),
                                initial_weight: current_weight, initial_bodyfat: current_bodyfat, current_bodyfat: current_bodyfat, activity_level: 1.2};
    
    person_config.write_to_file(config_file)?;
    
    let first_composition_data = composition::Composition{ weight: vec![Some(current_weight)], date: vec![Local::now().timestamp()], bodyfat: vec![current_bodyfat] };
    
    match first_composition_data.write_to_file(&comp_file) {
        Ok(_)   =>  Ok(()),
        Err(e)  =>  panic!("{:?}", e)
    }
  
}

fn cli_parse(input: &str)    ->  Result<String, std::io::Error>  {

    let mut output = String::new();

    print!("{}: ", input); //ask for INPUT
    std::io::stdout().flush()?;

    std::io::stdin().read_line(&mut output)?;

    Ok(remove_newline(output))
}

fn parse_birthday_input(input: &String) ->  Result<(u32, u32, i32), errors::BirthdayParseError> {
    let v: Vec<&str> = input.as_str().split(|c| c == '/' || c == '.' || c == ' ' || c == '\\' || c == '-').collect();
    if v.len() == 3 {
        let day = v[0].parse::<u32>()?;
        let month = v[1].parse::<u32>()?;
        let year = v[2].parse::<i32>()?;
        Ok((day, month, year))
    }
    else {
    Err(errors::BirthdayParseError::Input)
    }
}

fn parse_sex_input(input: &String) ->  Result<BiologicalSex, errors::InputParseError> {
    match input.as_ref() {
        "M" | "m" | "male"        =>  Ok(BiologicalSex::Male),
        "F" | "f" | "female"      =>  Ok(BiologicalSex::Female),
        _                   =>  Err(errors::InputParseError::NoMatch)
    }
}

fn parse_height_input(input: &String) ->  Result<u8, errors::InputParseError> {
    Ok(input.parse::<u8>()?)

}

fn parse_weight_input(input: &String) ->  Result<f64, errors::InputParseError> {
    match input.is_empty() {
        true    =>  return Err(errors::InputParseError::Empty),
        false   =>  ()
    }
    Ok(input.parse::<f64>()?)

}
