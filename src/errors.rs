extern crate custom_error;

use custom_error::custom_error;

custom_error! {pub BirthdayParseError
    Format{source: std::num::ParseIntError} = "not a valid integer",
    TooLarge{value:u8}            = "the number in the file ({value}) is too large",
    Input                           = "Not enough inputs or incorrect seperators"
}

custom_error! {pub InputParseError
    NoMatch = "No pattern match found",
    IntFormat{source: std::num::ParseIntError} = "not a valid integer",
    FloatFormat{source: std::num::ParseFloatError} = "not a valid number",
    Empty   =   "Passed value is empty"
}

custom_error! {pub PersonError
    WriteError{source: std::io::Error} = "Could not read/write file",
    SerializeError{source: serde_json::Error} = "Serde JSON error"
}

custom_error! {pub FileError
    TimeError{source: std::time::SystemTimeError}   =   "Error calculating current time",
        SerializeError{source: serde_json::Error} = "Serde JSON error",
            WriteError{source: std::io::Error} = "Could not read/write file",


}
