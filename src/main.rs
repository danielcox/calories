extern crate chrono;
extern crate clap;
extern crate colored;
extern crate dirs;
extern crate serde;
extern crate serde_derive;
extern crate serde_json;

use chrono::offset::LocalResult;
use chrono::prelude::*;
use clap::{App, Arg, SubCommand};
use colored::*;
use serde_derive::{Deserialize, Serialize};
use std::collections::HashMap;
use std::collections::HashSet;

use config::Person;
use food::ingredient::Unit;

mod composition;
mod config;
mod errors;
mod food;

fn pretty_print_weight(body_composition: composition::Composition) {
    let message = match body_composition.weight.iter().rposition(|&x| x.is_some()) {
        Some(i) => match body_composition.bodyfat[i] {
            Some(f) => format!(
                "[{} kg / {} %]",
                body_composition.weight[i].unwrap(),
                body_composition.bodyfat[i].unwrap()
            ),
            None => format!("[{} kg]", body_composition.weight[i].unwrap()),
        },
        None => format!("No weight data available"),
    };

    println!("{}", message.bright_yellow());
}

fn main() {
    let inputopts = App::new("Calories")
        .about("Add new weight data") // The message displayed in "myapp -h"
        // or "myapp help"
        .version("0.1") // Subcommands can have independent version
        .author("mrdjc") // And authors
        .subcommand(
            SubCommand::with_name("weight")
                .about("Used for configuration")
                .arg(
                    Arg::with_name("kilos")
                        .help("The configuration file to use")
                        .index(1),
                ),
        )
        .get_matches();

    let mut config_dir = dirs::config_dir().expect("Error: No user config directory found");
    config_dir.push("calories");

    match config::check_config_dir() {
        Some(_) => (),
        None => match config::make_config_dir(&config_dir) {
            Ok(_) => (),
            Err(e) => panic!("Error: Unable to create config directory\n{}", e),
        },
    }

    let config_file = config_dir.join("config.json");
    let comp_file = config_dir.join("comp.json");
    let food_file = config_dir.join("food.json");

    match config_file.exists() {
        true => (),
        false => match config::create_config_interactive(&config_file, &comp_file) {
            Ok(_) => (),
            Err(e) => panic!("{:?}", e),
        },
    }

    let person_info = match config::read_from_file(&config_file) {
        Ok(p) => p,
        Err(e) => panic!("Error reading from file"),
    };

    let mut body_composition = match comp_file.exists() {
        true => match composition::read_from_file(&comp_file) {
            Ok(f) => f,
            Err(e) => panic!("{:?}", e),
        },
        false => composition::Composition {
            weight: vec![Some(person_info.current_weight)],
            date: vec![Local::now().timestamp()],
            bodyfat: vec![None],
        },
    };

    pretty_print_weight(body_composition);

    if inputopts.is_present("weight") {
        println!("Weight: {:?}", inputopts.index_of("weight"));
    }

    println!("BMR: {}", person_info.bmr());
    println!("TDEE: {}", person_info.tdee());

    let mut ingredientlist = match food::ingredient::read_from_file(&config_dir.join("ingredients.json")) {
        Ok(i)    =>  i,
        Err(e)      =>  panic!(e),
    };

    //ingredientlist.write_to_file(&config_dir.join("ingredients.json"));

    //let fishpie = food::Recipe{name: "fish pie".to_string(), ingredients: [(onion.name, 250.0), (potato.name, 926.0), (milk.name, 500.0), (basa.name, 230.0), (prawns.name, 154.0), (fishmix.name, 1.0), (butter.name, 60.0), (flour.name, 100.0), (peas.name, 150.0), (eggs.name, 4.0)].iter().cloned().collect(), portions: 4.0 };

    let mut fishpie = food::recipe::PackedRecipe {
        name: "fish pie".to_string(),
        ingredients: HashMap::new(),
        portions: 4.0,
        weight: None,
    };

    fishpie.ingredients.insert(
        ingredientlist.find("onion").unwrap().name.clone(),
        200.0,
    );
    fishpie.ingredients.insert(ingredientlist.find("potato").unwrap()
            .name
            .clone(),
        926.0,
    );
    fishpie.ingredients.insert(
        ingredientlist.find("milk").unwrap().name.clone(),
        500.0,
    );
    fishpie.ingredients.insert(
        ingredientlist.find("basa fillet").unwrap().name.clone(),
        230.0,
    );
    fishpie.ingredients.insert(
        ingredientlist.find("prawns")
            .unwrap()
            .name
            .clone(),
        154.0,
    );
    fishpie.ingredients.insert(
        ingredientlist.find("fish mix")
            .unwrap()
            .name
            .clone(),
        1.0,
    );
    fishpie.ingredients.insert(
        ingredientlist.find("butter")
            .unwrap()
            .name
            .clone(),
        60.0,
    );
    fishpie.ingredients.insert(
        ingredientlist.find("flour").unwrap().name.clone(),
        100.0,
    );
    fishpie.ingredients.insert(
        ingredientlist.find("peas").unwrap().name.clone(),
        150.0,
    );
    fishpie.ingredients.insert(
        ingredientlist.find("egg (small)").unwrap().name.clone(),
        4.0,
    );

    let fp = fishpie.unpack(&ingredientlist);

    dbg!(fp.total_calories());
    dbg!(fp.portion_calories());
    let oliveoil = food::ingredient::Ingredient::new(
        String::from("olive oil"), 876.0, food::ingredient::Unit::DeciLiter, Some(0.15),
    None, None, None, None, None, None, None, None);
    
    let mut fish_curry = food::recipe::Recipe {
        name: String::from("fish curry"),
        ingredients: HashMap::new(),
        portions: 2.0,
        weight: None,
    };

    fish_curry.ingredients.insert( ingredientlist.find("onion").unwrap().clone(), 150.0);
    fish_curry.ingredients.insert( ingredientlist.find("brown basmati").unwrap().clone(), 160.0);
    fish_curry.ingredients.insert( ingredientlist.find("anchovy").unwrap().clone(), 30.0);
    fish_curry.ingredients.insert( ingredientlist.find("olive oil").unwrap().clone(), 20.0);
    fish_curry.ingredients.insert( ingredientlist.find("basa fillet").unwrap().clone(), 140.0);
    fish_curry.ingredients.insert( ingredientlist.find("coconut milk").unwrap().clone(), 400.0);
    fish_curry.ingredients.insert( ingredientlist.find("prawns").unwrap().clone(), 175.0);



    println!("Total calories: {}", fish_curry.total_calories());
    println!("Portion calories: {}", fish_curry.portion_calories());


    ingredientlist.write_to_file(&config_dir.join("ingredients.json"));

    let mut recipelist = food::recipe::RecipeList{ list: HashSet::new() };
    recipelist.insert(fp.pack());
    recipelist.write_to_file(&config_dir.join("recipes.json"));
}
